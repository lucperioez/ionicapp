import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { FirstRunPage } from '../pages';
import { Settings } from '../providers';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { Advertising } from '../providers';
import { Slides } from 'ionic-angular';
@Component({
  template: `
<ion-menu [content]="content" *ngIf="logged" class="main-content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Pages</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
        <ion-icon name="{{p.icon}}">&nbsp;&nbsp;&nbsp;</ion-icon>{{p.title}}
        </button>
        <button menuClose ion-item (click)="logout()" color="danger">
        <span style="color:white!important;"><ion-icon   name="close-circle">&nbsp;&nbsp;&nbsp;</ion-icon>Logout</span>
        </button>
       
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-header *ngIf="logged">

  <ion-navbar>
    <ion-buttons start>

    </ion-buttons>
    <button ion-button menuToggle>
      <ion-icon name="menu" class="menu-icon"></ion-icon>
    </button>
    <ion-title>
      {{Title}}
    </ion-title>
    <ion-buttons end>
      <button (click)="openPage({ title: 'Config', component: 'ConfigPage' })" ion-button>
        <ion-icon name="contact" class="porfile-icon" ></ion-icon>
      </button>
    </ion-buttons>
  </ion-navbar>

</ion-header>

  <ion-nav #content  [root]="rootPage"></ion-nav>
  
  <ion-footer *ngIf="logged">
  <ion-slides *ngIf="PImage && PImage.length" (ionSlideDidChange)="slideChanged()" autoplay="5000" loop="true" speed="2000" class="slides" pager="true">
  <ion-slide *ngFor="let Image of PImage">

     <img src="http://138.68.56.247/workapp-backend{{Image.url}}" alt="Product Image">
    </ion-slide>
  </ion-slides>
  </ion-footer>
  `
})
export class MyApp {
  @ViewChild(Slides) slides: Slides;
  rootPage = FirstRunPage;
  public Title: String;
  public logged:boolean;
  subscription:any;
  @ViewChild(Nav) nav: Nav;
  pages : any[];
  pages_buldier: any[] = [
    { title: 'FIND EMPLOYEES', component: 'SearchPage', icon: 'search' },
    { title: 'MY OFFERTS', component: 'OffertsPage', icon: 'clipboard' },
    { title: 'MY TEAM', component: 'MyTeamPage', icon: 'contacts' },
    { title: 'CONFIG', component: 'ConfigPage' , icon: 'settings'}
  ]

  pages_employee: any[] = [
    { title: 'FIND OFFERS', component: 'SearchOffersPage', icon: 'search' },
    { title: 'MY REFERENCES', component: 'MyReferencesPage', icon: 'checkmark-circle' },
    { title: 'RECOMENDED OFFERS', component: 'RecomendedOffersPage', icon: 'archive' },
    { title: 'CONFIG', component: 'ConfigPage', icon: 'settings' }
  ]
  PImage: any;
  constructor(public advertising: Advertising,public events: Events,private translate: TranslateService, platform: Platform, settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,public storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
 
  
    events.subscribe('update:menu', (Title) => {
      this.Title = Title;
      this.logged = true;
      console.log(Title);
      this.storage.get('usr').then((val) => {
        if(val != null && val != undefined){
          
          if(val){
            
            switch (parseInt(val.user.role_id)) {
              case 2:
                this.pages = this.pages_buldier;
                break;
              case 3:
                this.pages = this.pages_employee;
                break;
              default:
                break;
            }
          }
        }
      })
    });

    events.subscribe('close:menu', () => {
      this.logged = false;
      console.log('entro');
    });
    
    this.advertising.get().subscribe((resp) => {
      console.log(resp);
      this.PImage = resp;
    }, (err) => {

    });


  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();

      if(currentIndex == this.PImage.length){
        /*this.advertising.get().subscribe((resp) => {
          console.log(resp);
          this.PImage = resp;
        }, (err) => {
  
        });*/
      }

  }

  openPage(page) {
 
    this.nav.setRoot(page.component);
  }

  logout(){
    let TIME_IN_MS = 500;
    this.storage.remove('usr');
    setTimeout( () => {
      this.nav.setRoot('LoginPage');
    }, TIME_IN_MS);
    
    
  }
}
