import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { Offert } from '../../providers';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the OffertsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offerts',
  templateUrl: 'offerts.html',
})
export class OffertsPage {
  private id_user:any;
  private status:any;
  private offerts:any;
  public cities:any;
  public image:any;
  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public offert: Offert,
    public storage: Storage
    ) {
    this.status = "1";
    this.offerts = [];
    events.publish('update:menu','My Offerts');
    this.cities = ["Abbotsford",
    "Aldergrove",
    "Burnaby",
    "Chilliwack",
    "Coquitlam",
    "Surrey",
    "Maple Ridge",
    "Mission",
    "New Westminster",
    "North Vancouver",
    "White Rock",
    "Vancouver"];
    

  }

  ionViewDidLoad() {
    this.storage.get('usr').then((val) => {
      this.getSession(val); 
    })
    
  }

  getSession(val){
    this.id_user = val.user.id_user;
    this.image = val.user_detail.image;
    this.getOfferts();
  }

  addOffert() {
    this.navCtrl.push('OffertEditPage',{ type: 'add' });
  }

  editOffert(id) {
    this.navCtrl.push('OffertEditPage',{ type: 'edit', id: id });
  }

  getOfferts() {
    this.offert.getOffertByStatus(this.status,this.id_user).subscribe((resp) => {
      console.log(resp['offer'])
      this.offerts = resp['offer'];
    }, (err) => {

    });
  }




}
