import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../../providers';
import { Storage } from '@ionic/storage';
import { Employee } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private signupErrorString: string;
  private authForm : FormGroup;
  public cities:any;
  public province:any;
  constructor(
    public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    public storage: Storage,
    public employee: Employee
    ) {
      this.authForm = this.formBuilder.group({
        first_name:['', Validators.required],
        last_name: ['', Validators.required],
        phone: ['', Validators.required],
        company: ['', Validators.required],
        notifications: ['', Validators.required],
        user: ['', Validators.required],
        id_province:['', Validators.required],
        id_city:['', Validators.required],
        password:['', Validators.required],
        confirmPassword:['', Validators.required]
      },
      {validator: this.matchingPasswords('password', 'confirmPassword')});
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }
  ionViewDidLoad() {
    this.getProvinces();
  }
  matchingPasswords(passwordKey: string, confirmPasswordKey: string){
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  Signup() {
    let account_info = this.authForm.value;
  }

  getProvinces() {
    this.employee.getProvinces().subscribe((resp) => {
      console.log(resp)
      this.province = resp;
    }, (err) => {

    });
  }

  getCities(id_province){
    console.log(id_province);
   this.employee.getCities(id_province).subscribe((resp) => {
      console.log(resp)
      this.cities = resp;
    }, (err) => {

    });
  }
}
