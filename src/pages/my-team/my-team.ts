import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,AlertController } from 'ionic-angular';
import { User } from '../../providers';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the MyTeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-team',
  templateUrl: 'my-team.html',
})
export class MyTeamPage {
  teamMembers:any;
  id_user:any;
  constructor(
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public user: User,
    public alertCtrl: AlertController,
    public storage: Storage
    ) {
      this.teamMembers = [];
  }

  ionViewDidLoad() {
    this.events.publish('update:menu',"My team");
    this.storage.get('usr').then((val) => {
      this.getSession(val); 
    })
  
  }

  getSession(val){
    this.id_user = val.user.id_user;
    this.getMembers();
  }

  getMembers(){
    this.user.getTeam(this.id_user).subscribe((resp) => {
      console.log(resp);
      this.teamMembers = resp['team'];
    });
  }

  detail(id) {
    this.navCtrl.push('EmployeeDetailPage',{  id: id });
  }

  delete(id) {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure you want to delete this member of your team?',
      message: 'You can add it later to your team again if you like.',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
           
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.user.deleteTeamMember(id).subscribe((resp) => {
              this.getMembers();
              const toast = this.toastCtrl.create({
                message: 'The member has been removed from the team',
                duration: 3000,
                position: 'top'
              });
              toast.present();
            });
          }
        }
      ]
    });
    confirm.present();
  }

}
