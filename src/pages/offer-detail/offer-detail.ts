import { Component } from '@angular/core';
import { IonicPage, NavController,Events, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Offert } from '../../providers';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the OfferDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offer-detail',
  templateUrl: 'offer-detail.html',
})
export class OfferDetailPage {
  offer:any;
  constructor(public events: Events,public toastCtrl: ToastController,public offert: Offert,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
    this.offer = [];
  }

  ionViewDidLoad() {
    this.events.publish('update:menu',"Offer detail");
    this.getOffert(this.navParams.get('id'));
  }

  getOffert(id){
    this.offert.getOffert(2).subscribe((resp) => {
      console.log(resp);
      this.offer = resp['offer'];
      this.offer.city = this.offer.city.toString();
      console.log(this.offer.title);
    }, (err) => {

    });
    this.offert.getVacants(id).subscribe((resp) => {
      //this.vacants = resp;
    })
  }

  postulate(){
    const confirm = this.alertCtrl.create({
      title: 'Are you sure you want to postulate to this offer?',
      message: 'The builder will resive youre information.',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
           
          }
        },
        {
          text: 'Agree',
          handler: () => {
            const toast = this.toastCtrl.create({
              message: 'The builder has been notificated from youre postulate.',
              duration: 3000,
              position: 'top'
            });
            toast.present();
          
          }
        }
      ]
    });
    confirm.present();
  }
}
