import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { User,Offert } from '../../providers';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the TeamOfferPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team-offer',
  templateUrl: 'team-offer.html',
})
export class TeamOfferPage {
  teamMembers:any;
  id_user:any;
  type:any;
  teamCheck:any;
  checkAll:Boolean;
  constructor(public toastCtrl: ToastController,public loadingCtrl: LoadingController,public storage: Storage,public alertCtrl: AlertController,public user: User,public offer: Offert,public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get('usr').then((val) => {
      this.getSession(val); 
    })
    this.type = 'team';
  }

  ionViewDidLoad() {
    console.log('hola');
    
  }

  getSession(val){
    this.id_user = val.user.id_user;
    this.getMembers();
  }

  getMembers(){
    this.user.getTeam(this.id_user).subscribe((resp) => {
      console.log(resp);
      this.teamMembers = resp['team'];

    });
  }

  selectAllMembers(){
    if(this.checkAll){
      this.teamMembers.forEach(element => {
        element.employe.checked = true;
      });
    }else{
      this.teamMembers.forEach(element => {
        element.employe.checked = false;
      });
    }
  }
  publishToAll(){
    const toast = this.toastCtrl.create({
      message: 'The offer has been published now employees can see it',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }
  publishToTeam(){
    let array = this.teamMembers.filter(function(obj) {
      if(obj.employe.checked){
        return true; // skip
      }
      return false;
    }).map(function(obj){
        return {
          "team_id":obj.id_team,
          "offer_id":2
        }
    })
    if(array.length==0){
      let alert = this.alertCtrl.create({
        title: 'No members selected',
        subTitle: 'You must select a member',
        buttons: ['Ok']
      });
      alert.present();
    }else{
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                    Registrando datos espere un momento porfavor ...</p>`,
        duration: 5000
      });

      loading.present();
      array.forEach(element => {
        element.builder_id = this.id_user
      })
      this.offer.addOfferTeam({"offers_teams":array}).subscribe((resp) => {
        loading.dismiss();
      });
      loading.onDidDismiss(() => {
        const toast = this.toastCtrl.create({
          message: 'The offer has been sended to the selected members',
          duration: 3000,
          position: 'middle'
        });
        toast.present();
        this.navCtrl.setRoot('OffertsPage');
      });
      
    }


  }

}
