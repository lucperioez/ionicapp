import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamOfferPage } from './team-offer';

@NgModule({
  declarations: [
    TeamOfferPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamOfferPage),
  ],
})
export class TeamOfferPageModule {}
