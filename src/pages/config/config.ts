import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../../providers';

/**
 * Generated class for the ConfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  public imgPreview:any;
  public user_id:any;
  public user_session:any;
  public user_detail:any;
  public user_cities:any;
  
  private authForm : FormGroup;
  constructor(public events: Events,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,public storage: Storage,private camera: Camera,
    private formBuilder: FormBuilder,public user: User) {
    
    this.storage.get('usr').then((val) => {
      this.imprimir(val); 
    })

    this.authForm = this.formBuilder.group({
      first_name:['', ],
      last_name: ['', ],
      work_type: ['', ],
      user: ['', ],
      confirmPassword:['', ],
      cities:['', ],
      phone:['', ],
      description:['', ],
      experience:['', ]
    });


  }
  ionViewDidLoad() {
    console.log('hello');
    this.events.publish('update:menu','My acount');
  }

  imprimir(val){

    this.user_detail=val.user_detail;
    console.log(this.user_detail);
    if(this.user_detail.image != null){
      this.imgPreview = this.user_detail.image;
    }else{
      this.imgPreview = 'assets/imgs/avatar.png';
    }
 
    this.user_session=val.user;

    this.authForm.controls['first_name'].setValue(this.user_detail.first_name);
    this.authForm.controls['last_name'].setValue(this.user_detail.last_name);
    this.authForm.controls['work_type'].setValue(this.user_detail.work_type);
    this.authForm.controls['phone'].setValue(this.user_detail.phone);
    this.authForm.controls['user'].setValue(this.user_session.user);
    this.authForm.controls['cities'].setValue(this.user_detail.cities);
    this.authForm.controls['description'].setValue(this.user_detail.description);
    this.authForm.controls['experience'].setValue(this.user_detail.experience);
    this.user.getCities(this.user_detail.id_detail).subscribe((resp) => {
      this.user_cities = resp['employee_city'];
      let new_cities = this.user_cities.map(function(c){
        return c.city_id;
        
      });

      this.authForm.controls['cities'].setValue(new_cities);
    }, (err) => {
    });
  } 

  getPhoto() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL, //convertir en base64
      quality: 100,
      targetWidth: 150,
      targetHeight: 150,
      encodingType: this.camera.EncodingType.PNG,
    }
    
    this.camera.getPicture(options).then((imageData) => {
     
        return imageData;
      
    }).then((image) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + image;
     this.imgPreview = base64Image;
    }, (err) => {
     // Handle error
    });

    
  }


  update_info(){
    

    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                  Registrando datos espere un momento porfavor ...</p>`
    });
  
    loading.onDidDismiss(() => {
      this.navCtrl.setRoot('ConfigPage');
    });
  
    let userData = this.authForm.value;
    
    if(this.imgPreview == "assets/imgs/avatar.png"){
      userData.image = null;
    }else{
      userData.image = this.imgPreview;
    }
    
  
    this.user.signup_employee(userData,this.user_session.id_user).subscribe((resp) => {
      this.storage.set('usr', {user:this.user_session,user_detail:resp['user_detail']});
      this.user_detail=resp['user_detail'];
      console.log(this.user_detail);
      loading.dismiss();
    }, (err) => {
    });

    
    if(this.authForm.value.cities != undefined){
      let cities = this.authForm.value.cities.map(function(c){
        return {
          city_id:c
        }
      });
      this.user_cities.forEach(element => {
        this.user.deleteCities(element.id_employee_city).subscribe((resp) => {
        }, (err) => {
        });
      })
      
        cities.forEach(element => {
          this.user.cities(this.user_detail.id_detail,element).subscribe((resp) => {
           this.user.getCities(this.user_detail.id_detail).subscribe((resp) => {
              this.user_cities = resp['employee_city'];
              let new_cities = this.user_cities.map(function(c){
                return c.city_id;
                
              });
             
              this.authForm.controls['cities'].setValue(new_cities);
            }, (err) => {
            });
          }, (err) => {
           
          });
        });
     
        loading.present();
      
    }  
  


  }

 

}
