import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Offert } from '../../providers';
/**
 * Generated class for the VacantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vacant',
  templateUrl: 'vacant.html',
})
export class VacantPage {
  private authForm : FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public offert: Offert,
    private formBuilder: FormBuilder) {
      this.authForm = this.formBuilder.group({
        work_type:['', ],
        number:['', ]
      });
  }

  ionViewDidLoad() {
    console.log(this.navParams.get('id'));
  }

  save(){
    let vacant_data = this.authForm.value;
    vacant_data.status = 1;
    vacant_data.offer_id = this.navParams.get('id');
    vacant_data.status = 1;
    vacant_data.employe_id = '';
    vacant_data.skill_rank = '';
    vacant_data.puntuality_rank = '';
    vacant_data.dicipline_rank = '';
    vacant_data.bad_rank = '';
    vacant_data.end_hour = '';
    vacant_data.public_phone = '';

    this.offert.addVacant(vacant_data).subscribe((resp) => {
      console.log(resp);
      this.viewCtrl.dismiss(vacant_data);
    }, (err) => {
    });

    
  }
}
