import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Employee } from '../../providers';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the EmployeeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-employee-detail',
  templateUrl: 'employee-detail.html',
})
export class EmployeeDetailPage {
  id_user: any;
  employeeDetail: any;
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentDayValid: any;
  currentMonthValid: any;
  currentYearValid: any;
  currentYear: any;
  currentDate: any;
  reservedDays: any;
  constructor(public events: Events,public toastCtrl: ToastController,public storage: Storage,public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController,public employee: Employee) {

  }

  ionViewDidLoad() {
    this.events.publish('update:menu',"Employee detail");
    this.employee.getEmployeeDetail(this.navParams.get('id')).subscribe((resp) => {
      this.employeeDetail = resp['detail'];
    });

    this.storage.get('usr').then((val) => {
      this.getSession(val); 
    })
    
  }

  getSession(val){
    this.id_user = val.user.id_user;
  }

 
  ngOnInit() {
    this.date = new Date();
    this.reservedDays = {
      "initDate":"2018-09-01",
      "finishDate":"2018-11-15"
    };
    this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    this.getDaysOfMonth();
  }
 
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();

    this.currentMonth = this.monthNames[this.date.getMonth()];

    this.currentYear = this.date.getFullYear();

    if(this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
      console.log("Current");
      console.log(this.currentDate);
    } else {
      this.currentDate = 999;
    }
  
    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push({
        "NumberDay":i,
        "Date":this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth()-1, i))
      });

    }
  
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (var i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push({
        "NumberDay":i+1,
        "Date":this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth(), i+1))
      });

      
    }
  
    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var i = 0; i < (6-lastDayThisMonth); i++) {
      this.daysInNextMonth.push({
        "NumberDay":i+1,
        "Date":this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth()+1, i+1))
      });

      
    }

    var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays<36) {
      for(var i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++) {
        this.daysInNextMonth.push({
          "NumberDay":i+1,
          "Date":this.formatDate(new Date(this.date.getFullYear(), this.date.getMonth()+1, i+1))
        });
      }
    }

   
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    console.log(this.date);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    console.log(this.date);
    this.getDaysOfMonth();
  }
  
  contact(){
    const confirm = this.alertCtrl.create({
      title: 'Sure you want to contact?',
      message: 'We will send a notification to the employee',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  
  addToTeam() {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure you want to add this member to your team?',
      message: 'You can see it in the menu "MY TEAM".',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
           
          }
        },
        {
          text: 'Agree',
          handler: () => {
            let teamData = {
              "builder_id":this.id_user,
              "employee_id":this.navParams.get('id')
            };
            this.employee.addToTeam(teamData).subscribe((resp) => {
              const toast = this.toastCtrl.create({
                message: 'The member has been added to youre the team',
                duration: 3000,
                position: 'top'
              });
              toast.present();
            });
          }
        }
      ]
    });
    confirm.present();

  }

}
