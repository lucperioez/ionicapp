import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchOffersPage } from './search-offers';

@NgModule({
  declarations: [
    SearchOffersPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchOffersPage),
  ],
})
export class SearchOffersPageModule {}
