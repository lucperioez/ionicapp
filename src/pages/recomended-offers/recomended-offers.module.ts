import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecomendedOffersPage } from './recomended-offers';

@NgModule({
  declarations: [
    RecomendedOffersPage,
  ],
  imports: [
    IonicPageModule.forChild(RecomendedOffersPage),
  ],
})
export class RecomendedOffersPageModule {}
