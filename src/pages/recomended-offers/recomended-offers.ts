import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Offert } from '../../providers';
/**
 * Generated class for the RecomendedOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recomended-offers',
  templateUrl: 'recomended-offers.html',
})
export class RecomendedOffersPage {
  offers:any;
  constructor(public offert: Offert,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.offert.getOffertByEmployee(1).subscribe((resp) => {
      console.log(resp);
      this.offers = resp['team'];
    })
  }

  offerDetail(id){ 
    this.navCtrl.push('OfferDetailPage',{ id: id });
   
  }

}
