import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingupEmployeePage } from './singup-employee';

@NgModule({
  declarations: [
    SingupEmployeePage,
  ],
  imports: [
    IonicPageModule.forChild(SingupEmployeePage),
  ],
})
export class SingupEmployeePageModule {}
