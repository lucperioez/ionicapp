import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../../providers';
import { Storage } from '@ionic/storage';
import { Employee } from '../../providers';
/**
 * Generated class for the SingupEmployeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-singup-employee',
  templateUrl: 'singup-employee.html',
})
export class SingupEmployeePage {
  
  public options:CameraOptions;
  public province:any;
  public cities:any;
  public imgPreview:any;
  private authForm : FormGroup;
  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,private camera: Camera,private imagePicker: ImagePicker,
    private file: File,private formBuilder: FormBuilder,public user: User,public storage: Storage, public employee:Employee) {
    this.authForm = this.formBuilder.group({
      first_name:['', Validators.required],
      last_name: ['', Validators.required],
      work_type: ['', Validators.required],
      id_province:['', Validators.required],
      id_city:['', Validators.required],
      user: ['', Validators.required],
      password:['', Validators.required],
      confirmPassword:['', Validators.required]
    },
    {validator: this.matchingPasswords('password', 'confirmPassword')});
    //this.imgPreview = 'assets/imgs/logo.png';
 
  }
  ionViewDidLoad() {
      this.getProvinces();
  }
  


  getPhoto() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL, //convertir en base64
      quality: 100,
    encodingType: this.camera.EncodingType.PNG,
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     this.imgPreview = base64Image;
    }, (err) => {
     // Handle error
    });

    
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string){
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  singUp(){
    let account_info = this.authForm.value;
    account_info.role_id = 3;
    this.user.first_signup_employee(this.authForm.value).subscribe((resp) => {
      this.storage.set('usr', {user:resp['user'],user_detail:resp['user_detail']});
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                    Registrando datos espere un momento porfavor ...</p>`,
        duration: 5000
      });
    
      loading.onDidDismiss(() => {
        this.navCtrl.setRoot('ConfigPage');
      });
    
      loading.present();
     
    }, (err) => {

    });
  }

  getProvinces() {
    this.employee.getProvinces().subscribe((resp) => {
      console.log(resp)
      this.province = resp;
    }, (err) => {

    });
  }

  getCities(id_province){
    console.log(id_province);
   this.employee.getCities(id_province).subscribe((resp) => {
      console.log(resp)
      this.cities = resp;
    }, (err) => {

    });
  }

  

}
