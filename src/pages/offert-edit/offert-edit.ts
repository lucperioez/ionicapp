import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,LoadingController,ToastController,ModalController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Offert } from '../../providers';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the OffertEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offert-edit',
  templateUrl: 'offert-edit.html',
})
export class OffertEditPage {
  private authForm : FormGroup;
  private Title : any;
  private id_user : any;
  private vacants : any;
  public offer : any;
  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public offert: Offert,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController
     ) {
    this.offer = {status: 0};
    this.Title = 'Add Offert';
    this.authForm = this.formBuilder.group({
      city:['', ],
      address:['', ],
      hour_price:['', ],
      start_date:['', ],
      end_date:['', ],
      start_hour:['', ],
      end_hour:['', ],
      description:['', ],
      title:['', ],
    });
    events.publish('update:menu',this.Title);

    this.storage.get('usr').then((val) => {
      this.getSession(val); 
    })
  }

  getSession(val){
    this.id_user = val.user.id_user;
  }

  ionViewDidLoad() {
    if(this.navParams.get('type') == 'edit'){
      this.getOffert(this.navParams.get('id'));
    }
  }
  getOffert(id){
    this.offert.getOffert(id).subscribe((resp) => {
      console.log(resp);
      this.offer = resp['offer'];
      console.log(this.offer);
      let offer_data = resp['offer'];
      this.authForm.controls['city'].setValue(offer_data.city);
      this.authForm.controls['address'].setValue(offer_data.address);
      this.authForm.controls['hour_price'].setValue(offer_data.hour_price);
      this.authForm.controls['start_date'].setValue(offer_data.start_date);
      this.authForm.controls['end_date'].setValue(offer_data.end_date);
      this.authForm.controls['start_hour'].setValue(offer_data.start_hour);
      this.authForm.controls['end_hour'].setValue(offer_data.end_hour);
      this.authForm.controls['description'].setValue(offer_data.description);
      this.authForm.controls['title'].setValue(offer_data.title);
    }, (err) => {

    });
    this.offert.getVacants(id).subscribe((resp) => {
      this.vacants = resp;
    })
  }
  save(){
    let offer_data = this.authForm.value;
    if(this.navParams.get('type') == 'edit'){
      offer_data.builder_id = this.id_user;
      console.log(offer_data);
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                    Registrando datos espere un momento porfavor ...</p>`,
        duration: 5000
      });
      loading.onDidDismiss(() => {
        this.getOffert(this.navParams.get('id'));
        let toast = this.toastCtrl.create({
          message: 'Offer update success',
          duration: 3000,
          position: 'top'
        });
      
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
      
      });
      loading.present();
      this.offert.update(this.navParams.get('id'),offer_data).subscribe((resp) => {
        loading.dismiss();
      }, (err) => {
  
      });
    }else{
      
      offer_data.builder_id = this.id_user;
      console.log(offer_data);
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                    Registrando datos espere un momento porfavor ...</p>`,
        duration: 5000
      });
      loading.onDidDismiss(() => {
        this.navCtrl.push('OffertsPage');
      });
      loading.present();
      this.offert.add(offer_data).subscribe((resp) => {
        loading.dismiss();
      }, (err) => {
  
      });
    }
  }


  addVacant(){
    let profileModal = this.modalCtrl.create('VacantPage', { id: this.navParams.get('id') });
    profileModal.onDidDismiss(data => {
      console.log(data);
     
    });
    profileModal.present();
  }

  publish(){
    let profileModal = this.modalCtrl.create('TeamOfferPage', { id: this.navParams.get('id') });
    profileModal.onDidDismiss(data => {
      console.log(data);
     
    });
    profileModal.present();
  }
 


}


