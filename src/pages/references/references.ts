import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Employee } from '../../providers';
/**
 * Generated class for the ReferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-references',
  templateUrl: 'references.html',
})
export class ReferencesPage {

  public refrences:any;
  constructor(public employee: Employee,public navCtrl: NavController, public navParams: NavParams,private callNumber: CallNumber) {
  }

  ionViewDidLoad() {
    this.employee.getReferences(this.navParams.get('id')).subscribe((resp) => {
      console.log(resp);
      this.refrences = resp['refrences'];
    });
  }

  call(number){
    setTimeout(() => {
      let tel = number;
      window.open(`tel:${tel}`, '_system');
    },100);
  }

}
