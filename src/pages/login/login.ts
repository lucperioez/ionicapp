import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { User } from '../../providers';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public events: Events,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public loadingCtrl: LoadingController,
    public storage: Storage) {    
    events.publish('close:menu');
  }

  ionViewDidLoad() {

    this.storage.get('usr').then((val) => {
      if(val){
        switch (val.user.role_id) {
          case '2':
            this.navCtrl.setRoot('SearchPage');
            break;
          case '3':
            this.navCtrl.setRoot('SearchOffersPage');
            break;
          default:
            break;
        }
        
      }

    });
  }

  signupBuldier() {
    this.navCtrl.push('SignupPage');
  }

  signupEmployee(){
    this.navCtrl.push('SingupEmployeePage');
  }
  
  // Attempt to login in through our User service
  doLogin() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                  Registrando datos espere un momento porfavor ...</p>`
    });
    loading.present();
 

    this.user.login(this.account).subscribe((resp) => {
  
      this.storage.set('usr', {user:resp['user'],user_detail:resp['user_detail']});
      switch (resp['user'].role_id) {
        case '2':
          this.navCtrl.setRoot('SearchPage');
          break;
        case '3':
          this.navCtrl.setRoot('SearchOffersPage');
          break;
        default:
          break;
      }
      loading.dismiss();
    }, (err) => {
      loading.dismiss();
      this.loginErrorString = err;
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: "Usuario o contraseña incorrecta",
        duration: 3000,
        position: 'top'
      });
      toast.present();

    });
  }

  register(){
    this.navCtrl.setRoot('WelcomePage');
  }
}
