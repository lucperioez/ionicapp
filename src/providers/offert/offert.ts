import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class Offert {


  constructor(public api: Api) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  add(data: any) {
    let seq = this.api.post('offers/', data).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  update(id:any,data:any) {
    let seq = this.api.put('offers/'+id,data).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }


  get(id:any) {
    let seq = this.api.get('offers/builder/'+id).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getOffert(id:any) {
    let seq = this.api.get('offers/'+id).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getOffertByStatus(status:any,id_builder:any) {
    let seq = this.api.get('offers/status/'+status+'/'+id_builder).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getVacants(id:any) {
    let seq = this.api.get('plaza/offer/'+id).share();
    return seq;
  }

  addVacant(data:any) {
    let seq = this.api.post('plaza/',data).share();
    return seq;
  }

  addOfferTeam(data:any) {
    let seq = this.api.post('offerteam/',data).share();
    return seq;
  }

  getOffertByEmployee(id_employee:any) {
    let seq = this.api.get('offerteam/employe/'+id_employee).share();
    return seq;
  }

}
